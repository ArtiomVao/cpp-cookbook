#include "dummy_array.h"
#include <iostream>
#include <iterator>
#include <memory>
#include <utility>
#include <vector>

using namespace da;

class person {
public:
  std::string name;
  int age;
  person(std::string n, int a) : name(n), age(a) {
    std::cout << "init person"
              << "\n";
  };
  ~person() {
    std::cout << "deinit person"
              << "\n";
  };
};

class foo {
public:
  int id;
  std::string name;
  std::vector<int> vec;
  person *obj;

  foo(int i, std::string n, std::vector<int> v, person *p)
      : id(i), name(n), vec(v), obj(p) {
    std::cout << "init foo"
              << "\n";
  }

  foo(const foo &f) : id(f.id), name(f.name), vec(f.vec), obj(f.obj) {
    std::cout << "init foo copy"
              << "\n";
  }

  // foo(foo &&other) = default;

  foo(foo &&other)
      : id(other.id), name(std::move(other.name)), vec(std::move(other.vec)),
        obj(std::move(other.obj)) {
    std::cout << "init foo move"
              << "\n";
    other.obj = nullptr;
  }

  // foo &operator=(foo &&other)
  // {
  // }

  ~foo() {
    std::cout << "deinit foo"
              << "\n";
  }
};

void c1() {
  std::cout << "Hello"
            << "\n";

  dummy_array<int, 3> arr;

  arr.SetAt(0, 1);
  arr.SetAt(1, 2);
  arr.SetAt(2, 33);

  for (auto &&e : arr) {
    auto s = std::move(e);
    std::cout << "arr: " << s << "\n";
  }

  std::cout << "arr[0]: " << arr.GetAt(0) << "\n";
  auto p = person{"trash", 19};
  foo f{42, "john", {1, 2, 3}, &p};

  // auto [id, name] = f;

  foo f2(std::move(f));

  std::cout << "id: " << f.id << " name: " << f.name << "\n";

  auto lambda = [&f, arr]() { return f.id + arr.GetAt(2); };

  auto lambdaRes = lambda();
  std::cout << lambdaRes << '\n';

  // auto obj{*f.obj};
  std::cout << f.vec.size() << "\n";
  std::cout << f2.vec.size() << "\n";
  std::cout << (*f2.obj).name << "\n";
}
