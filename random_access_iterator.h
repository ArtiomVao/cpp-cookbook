#include <cassert>
#include <cstddef>
#include <functional>
#include <iterator>
#include <stdexcept>

template <typename T, size_t SIZE> class my_array_iterator {
public:
  typedef my_array_iterator self_type;
  typedef T value_type;
  typedef T &reference;
  typedef T *pointer;
  typedef std::random_access_iterator_tag iterator_category;
  typedef ptrdiff_t difference_type;

  explicit my_array_iterator(pointer ptr, size_t const index)
      : ptr(ptr), index(index) {}

  my_array_iterator(my_array_iterator const &o) = default;
  my_array_iterator &operator=(my_array_iterator const &o) = default;
  ~my_array_iterator() = default;

  self_type &operator++() {
    if (index >= SIZE)
      throw std::out_of_range(
          "Iterator cannot be incremented past the end of range.");
    ++index;
    return *this;
  }

  self_type operator++(int) {
    self_type tmp = *this;
    ++*this;
    return tmp;
  }

  bool operator==(self_type const &other) const {
    assert(compatible(other));
    return index == other.index;
  }

  bool operator!=(self_type const &other) const { return !(*this == other); }

  reference operator*() const {
    if (ptr == nullptr)
      throw std::bad_function_call();
    return *(ptr + index);
  }

  reference operator->() const {
    if (ptr == nullptr)
      throw std::bad_function_call();
    return *(ptr + index);
  }

  my_array_iterator() = default;

  self_type &operator--() {
    if (index <= 0)
      throw std::out_of_range(
          "Iterator cannot be decremented past the end of range.");
    --index;
    return *this;
  }

  self_type &operator--(int) {
    self_type tmp = *this;
    --*this;
    return tmp;
  }

  self_type operator+(difference_type offset) const {
    self_type tmp = *this;
    return tmp += offset;
  }

  self_type operator-(difference_type offset) const {
    self_type tmp = *this;
    return tmp -= offset;
  }

  difference_type operator-(self_type const &other) const {
    assert(compatible(other));
    return (index - other.index);
  }

  bool operator<(self_type const &other) const {
    assert(compatible(other));
    return index < other.index;
  }

  bool operator>(self_type const &other) const {
    assert(compatible(other));
    return other < *this;
  }

  bool operator<=(self_type const &other) const {
    assert(compatible(other));
    return !(other < *this);
  }

  bool operator>=(self_type const &other) const {
    assert(compatible(other));
    return !(*this < other);
  }

  self_type &operator+=(difference_type const offset) {
    if (index + offset < 0 || index + offset > SIZE)
      throw std::out_of_range(
          "Iterator cannot be incremented pat the end of range.");
    index += offset;
    return *this;
  }

  self_type &operator-=(difference_type const offset) {
    return *this += -offset;
  }

  value_type &operator[](difference_type const offset) {
    return (*(*this + offset));
  }

  value_type const &operator[](difference_type const offset) const {
    return (*(this + offset));
  }

private:
  pointer ptr = nullptr;
  size_t index = 0;

  bool compatible(self_type const &other) const { return ptr == other.ptr; }
};

template <typename T, size_t const SIZE> class my_array {
  T data[SIZE] = {};

public:
  typedef my_array_iterator<T, SIZE> iterator;
  typedef my_array_iterator<T const, SIZE> constant_iterator;

  T &operator[](size_t const index) {
    if (index < SIZE)
      return data[index];
    throw std::out_of_range("index out of range");
  }

  T const &operator[](size_t const index) const {
    if (index < SIZE)
      return data[index];
    throw std::out_of_range("index out of range");
  }

  size_t size() const { return SIZE; }

  iterator begin() { return iterator(data, 0); }

  iterator end() { return iterator(data, SIZE); }

  constant_iterator begin() const { return constant_iterator(data, 0); }

  constant_iterator end() const { return constant_iterator(data, SIZE); }
};
