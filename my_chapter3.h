#include "random_access_iterator.h"
#include <algorithm>
#include <iostream>
#include <iterator>
#include <memory>
#include <string>
#include <utility>
#include <vector>

template <unsigned n> struct B {
  void p() {
    auto res = 5 + n;
    std::cout << res << "\n";
  }
};

struct Tag {
  int num;
  std::string name;

  Tag(int num, std::string name) : num(num), name(name) {}
};

void c3() {
  B<5> b1;
  B<'c'> b2;
  b1.p();
  b2.p();

  std::vector<int> v{1, 1, 2, 3, 5, 8, 13};
  auto it =
      std::find_if(v.cbegin(), v.cend(), [](int const n) { return n > 10; });
  if (it != v.cend()) {
    std::cout << "index: " << std::distance(v.cbegin(), it) << " value: " << *it
              << "\n";
  }

  my_array<int, 3> a;
  a[0] = 10;
  a[1] = 20;
  a[2] = 30;

  std::transform(a.begin(), a.end(), a.begin(),
                 [](int const e) { return e * 2; });
  for (auto &&e : a)
    std::cout << e << '\n';

  auto lp = [](my_array<int, 3> const &ca) {
    for (auto const &e : ca)
      std::cout << e << '\n';
  };

  lp(a);

  my_array<std::unique_ptr<Tag>, 3> ta;
  ta[0] = std::make_unique<Tag>(1, "Tag 1");
  ta[1] = std::make_unique<Tag>(2, "Tag 2");
  ta[2] = std::make_unique<Tag>(3, "Tag 3");

  for (auto it = ta.begin(); it != ta.end(); ++it)
    std::cout << it->num << " " << it->name << '\n';

  std::vector<int> v1{1, 2, 3, 4, 5};
  auto sv1 = std::size(v1);
  auto ev1 = std::empty(v1);
  auto dv1 = std::data(v1);
  std::cout << "v1 size: " << sv1 << '\n';
  std::cout << "v1 is empty: " << ev1 << '\n';
  std::cout << "v1 data: " << dv1 << '\n';

  for (auto i = std::begin(v1); i != std::end(v1); ++i)
    std::cout << *i << '\n';
  std::vector<int> v2;
  std::copy(std::cbegin(v1), std::cend(v1), std::back_inserter(v2));
  for (auto i = std::begin(v2); i != std::end(v2); ++i)
    std::cout << *i << '\n';

  my_array<std::string, 5> ma;

  std::transform(std::begin(v1), std::end(v1), std::begin(ma),
                 [](int const &n) { return std::to_string(n); });
  std::cout << std::size(ma) << '\n';
}
