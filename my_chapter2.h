#include <iostream>
#include <utility>

template <typename T,
          typename = typename std::enable_if_t<std::is_integral_v<T>, T>>
auto mul(T const a, T const b) {
  return a * b;
}

template <typename T,
          typename = typename std::enable_if_t<!std::is_integral_v<T>, T>,
          typename = void>
auto mul(T const a, T const b) {
  return a - b;
}

void c2() {
  std::cout << "C2"
            << "\n";

  auto a = mul(3, 2);
  auto b = mul(3.0, 2.0);

  std::cout << a << "\n";
  std::cout << b << "\n";
}
