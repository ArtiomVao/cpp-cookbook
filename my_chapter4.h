#include <chrono>
#include <iostream>
#include <ratio>
void c4() {
  auto start = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < 1000000000; ++i)
    ;
  auto diff = std::chrono::high_resolution_clock::now() - start;
  std::cout << std::chrono::duration<double, std::milli>(diff).count() << "ms"
            << '\n';
}
